//+------------------------------------------------------------------+
//|                                              Economic Events.mq4 |
//|                                  Copyright 2019, Vladimir Klevko |
//|           https://gitlab.com/economic-events/economic-events-mt4 |
//+------------------------------------------------------------------+
#property strict

#include "EE.Structures.mqh"
#include "EE.Logger.mqh"
#include "lib/Utils/File.mqh"
#include "lib/Lang/Pointer.mqh"
#include "EE.EventSorter.mqh"
#include "EE.Graphics.mqh"

#define ONE_WEEK 604800
#define ONE_DAY 86400
#define ONE_HOUR 3600
#define DATA_FILE_NAME "events.csv"


#property indicator_chart_window
#property indicator_buffers 3

#property indicator_type1 DRAW_NONE
#property indicator_label1 "Low Volatility"

#property indicator_type2 DRAW_NONE
#property indicator_label2 "Moderate Volatility"

#property indicator_type2 DRAW_NONE
#property indicator_label3 "High Volatility"

double LowVolatilityEventBuffer[];
double ModerateVolatilityEventBuffer[];
double HighVolatilityEventBuffer[];

input bool IncludeLowVolatilityNews = false;      // Include Low Volatility Events
input bool IncludeModerateVolatilityNews = false; // Include Moderate Volatility Events
input bool IncludeHighVolatilityNews = true;      // Include High Volatility Events

input string DataPath = "Economic Data\\data\\"; // Path to the location of events data

input TimeZone BrokerTimeZone = GMT; // Broker Time Zone

input bool VisualizeEvents = true;

input ENUM_LOG_LEVEL logLevel = INFO; //Log level output
input bool logSpoolToStdOut = true;    // Log spool to journal

EconomicEvent EventsBuffer[];
Logger *log;

string baseCurrency = SymbolInfoString(Symbol(), SYMBOL_CURRENCY_BASE);
string quoteCurrency = StringSubstr(Symbol(), 3, 3);

string baseCurrencyDataLocation = StringConcatenate(DataPath, baseCurrency);
string quoteCurrencyDataLocation = StringConcatenate(DataPath, quoteCurrency);
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
  //--- indicator buffers mapping
  if (!IsDllsAllowed())
  {
    Print("Please allow DLL import for this indicator in terminal settings for: ", __FILE__);
    return (INIT_FAILED);
  }

  log = new Logger(StringConcatenate(__FILE__, ".log"), logLevel, logSpoolToStdOut);

  log.DEBUG("Base currency: ", baseCurrency, " Quote currency: ", quoteCurrency);
  log.DEBUG("Base currency data base URL: ", baseCurrencyDataLocation);
  log.DEBUG("Quote currency data base URL: ", quoteCurrencyDataLocation);

  ArrayInitialize(LowVolatilityEventBuffer, EMPTY_VALUE);
  ArrayInitialize(ModerateVolatilityEventBuffer, EMPTY_VALUE);
  ArrayInitialize(HighVolatilityEventBuffer, EMPTY_VALUE);

  SetIndexBuffer(0, LowVolatilityEventBuffer, INDICATOR_DATA);
  SetIndexBuffer(1, ModerateVolatilityEventBuffer, INDICATOR_DATA);
  SetIndexBuffer(2, HighVolatilityEventBuffer, INDICATOR_DATA);

  IndicatorShortName("Economic Events");

  //---
  return (INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
  SafeDelete(log);
  ClearEventsVizualizations();
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
  //---
  if (IsNewDay() || ArraySize(EventsBuffer) == 0)
  {
    Print("Loading events data to the cache.");
    LoadEvents();
  }
  if (VisualizeEvents)
  {
    ClearEventsVizualizations();
    VisualizeEvents(EventsBuffer);
  }
  if (IncludeLowVolatilityNews)
  {
    EconomicEvent event = GetEarliestEventByType(LOW);
    log.DEBUG(__FUNCTION__, " Closest low volatility event: ", EconomicEventToString(event));
    double minutesToEvent = GetMinutesUntilEvent(event);
    log.DEBUG(__FUNCTION__, " Minutes left until the event: ", DoubleToString(minutesToEvent, 0));
    LowVolatilityEventBuffer[0] = minutesToEvent;
  }
  if (IncludeModerateVolatilityNews)
  {
    EconomicEvent event = GetEarliestEventByType(MODERATE);
    log.DEBUG(__FUNCTION__, " Closest moderate volatility event: ", EconomicEventToString(event));
    double minutesToEvent = GetMinutesUntilEvent(event);
    log.DEBUG(__FUNCTION__, " Minutes left until the event: ", DoubleToString(minutesToEvent, 0));
    ModerateVolatilityEventBuffer[0] = minutesToEvent;
  }
  if (IncludeHighVolatilityNews)
  {
    EconomicEvent event = GetEarliestEventByType(HIGH);
    Print(__FUNCTION__, " Closest high volatility event: ", EconomicEventToString(event));
    double minutesToEvent = GetMinutesUntilEvent(event);
    Print (__FUNCTION__, " Minutes left until the event: ", DoubleToString(minutesToEvent, 0));
    HighVolatilityEventBuffer[0] = minutesToEvent;
  }

  //--- return value of prev_calculated for next call
  return (rates_total);
}

double GetMinutesUntilEvent(EconomicEvent &event)
{
  datetime timeNow = TimeCurrent();
  double minutesToevent = NormalizeDouble((event.eventDateTime - timeNow) / 60, 0);
  return minutesToevent;
}

EconomicEvent GetEarliestEventByType(ExpectedVolatility expectedVolatilityType)
{
  for (int i = 0; i < ArraySize(EventsBuffer); i++)
  {
    if (EventsBuffer[i].expectedVolatility == expectedVolatilityType && EventsBuffer[i].eventDateTime >= TimeCurrent())
    {
      return EventsBuffer[i];
    }
  }
  EconomicEvent emptyEvent = {"", "", "", 0, "", "", "", "", UNKNOWN};
  return emptyEvent;
}

void LoadEvents()
{
  Print("Building cache from: ", TimeToString(TimeCurrent()));
  ArrayResize(EventsBuffer, 0);

  datetime current = TimeCurrent();
  datetime weekAhead = current + ONE_WEEK;

  Print("Loading data from: ", TimeToString(current), " to: ", TimeToString(weekAhead));

  //string eventsData;

  while (current <= weekAhead)
  {
    Print("Fetchind data for dd/mm/yyyy: ", TimeDay(current), "/", TimeMonth(current), "/", TimeYear(current));
    string baseCurrencyData = StringConcatenate(baseCurrencyDataLocation, "\\", TimeYear(current), "\\", TimeMonth(current), "\\", TimeDay(current), "\\", DATA_FILE_NAME);
    string quoteCurrencyData = StringConcatenate(quoteCurrencyDataLocation, "\\", TimeYear(current), "\\", TimeMonth(current), "\\", TimeDay(current), "\\", DATA_FILE_NAME);
    Print("Base currency fetch location: ", baseCurrencyData);
    Print("Quote currency fetch location: ", quoteCurrencyData);
    ReadEvents(baseCurrencyData, EventsBuffer);
    ReadEvents(quoteCurrencyData, EventsBuffer);
    current += ONE_DAY;
  }

  Print("Number of events in buffer: ", ArraySize(EventsBuffer));
}

bool IsNewDay()
{
  static datetime bodCurr;
  datetime bodPrev = bodCurr;
  bodCurr = DateOfDay(TimeCurrent());
  return bodPrev != bodCurr;
}

datetime TimeOfDay(datetime when) { return (when % ONE_DAY); }
datetime DateOfDay(datetime when) { return (when - TimeOfDay(when)); }

void ReadEvents(string path, EconomicEvent &eventsBufferRef[])
{
  log.DEBUG(__FUNCTION__, " Reading ", path);
  CsvFile csv(path, FILE_READ | FILE_SHARE_READ, ';');

  if (!csv.valid())
  {
    log.WARN(__FUNCTION__, "Unable to read the following file: ", path);
    return;
  }

  for (int i = 0; !csv.end(); i++)
  {

    string eventName = csv.readString();
    string eventCountry = csv.readString();
    string eventCurrency = csv.readString();
    string eventDateTime = csv.readString();
    string eventTimeZone = csv.readString();
    string eventPrevious = csv.readString();
    string eventForecast = csv.readString();
    string eventActual = csv.readString();
    string expectedVolatility = csv.readString();

    StringReplace(eventName, "\"", "");
    StringReplace(eventCountry, "\"", "");
    StringReplace(eventCurrency, "\"", "");
    StringReplace(eventDateTime, "\"", "");
    StringReplace(eventTimeZone, "\"", "");
    StringReplace(eventPrevious, "\"", "");
    StringReplace(eventForecast, "\"", "");
    StringReplace(eventActual, "\"", "");
    StringReplace(expectedVolatility, "\"", "");

    log.DEBUG(__FUNCTION__, " Expected volatility: ", expectedVolatility);
    log.DEBUG(__FUNCTION__, " Expected volatility: ", EnumToString(ExpectedVolatilityFromString(expectedVolatility)));

    if (ExpectedVolatilityFromString(expectedVolatility) == LOW && !IncludeLowVolatilityNews)
    {
      log.DEBUG(__FUNCTION__, "Skipping Low volatility news.");
      continue;
    }

    if (ExpectedVolatilityFromString(expectedVolatility) == MODERATE && !IncludeModerateVolatilityNews)
    {
      log.DEBUG(__FUNCTION__, "Skipping Moderate volatility news.");
      continue;
    }

    if (ExpectedVolatilityFromString(expectedVolatility) == HIGH && !IncludeHighVolatilityNews)
    {
      log.DEBUG(__FUNCTION__, "Skipping High volatility news.");
      continue;
    }

    ArrayResize(eventsBufferRef, ArraySize(eventsBufferRef) + 1);

    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventName = eventName;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventCountry = eventCountry;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventCurrency = eventCurrency;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventDateTime = DatetimeFromString(eventDateTime);
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventTimeZone = eventTimeZone;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventPrevious = eventPrevious;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventForecast = eventForecast;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].eventActual = eventActual;
    eventsBufferRef[ArraySize(eventsBufferRef) - 1].expectedVolatility = ExpectedVolatilityFromString(expectedVolatility);

    log.DEBUG(__FUNCTION__, " Following entry has been added: ", EconomicEventToString(eventsBufferRef[ArraySize(eventsBufferRef) - 1]), " with index: ", IntegerToString(ArraySize(eventsBufferRef) - 1));
  }

  //Sort events by the date and time of the event after reading
  EventsSorter(eventsBufferRef, eventDateTime);

  // Trace output for the checks.
  for (int j = 0; j < ArraySize(eventsBufferRef); j++)
  {
    log.TRACE(__FUNCTION__, "Event with the index: ", IntegerToString(j), ": ", EconomicEventToString(eventsBufferRef[j]));
  }
}

datetime DatetimeFromString(string dateTimeString)
{
  // Date format: 2000-01-01T09:00:00 (yyyy-mm-ddThh:mi:ss) Target format (yyyy.mm.dd hh:mi): 2000.01.01 09:00
  int numberOfReplacements = StringReplace(dateTimeString, "T", " ");
  numberOfReplacements += StringReplace(dateTimeString, "-", ".");
  string finalDataString = StringSubstr(dateTimeString, 0, StringLen(dateTimeString) - 3);
  Print(__FUNCTION__, " Final date string: ", finalDataString);
  if (numberOfReplacements == 3 && StringLen(finalDataString) == 16)
  {
    return StringToTime(finalDataString) + BrokerTimeZone * ONE_HOUR;
  }
  else
  {
    return 0;
  }
}