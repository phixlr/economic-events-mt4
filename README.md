# Economic Events MT4

Meta Trader 4 indicator to work with the economic data.

# How to build the indicator

Open main indicator file (`Economic Events.mq4`) in the MetaEditor and press `F7` in order for the MetaEditor to build the code. After you have done that resulting `Economic Events.ex4` will become available in the same directory.

# Pre-requisites to use

Data from [Economic Events Data](https://gitlab.com/economic-events/economic-events-data) should be downloaded or cloned to a folder depending on where you intend to use indicator from.

For Strategy Tester:

`<METATRADER_DATA_FOLDER>/Tester/Files`

For Live:

`<META_TRADER_DATA_FOLDER>/MQL/Files`

Note the folder name that the data was copied. You'll need this folder to set the `Path to the location of events data` parameter of the indicator. Make sure that the relative path points to the `data` folder (a folder where the child folders has names to corresponding currencies) where the trailing `\\` or `/` is **important**.

Default value is set to `Economic Data\\data\\` so that the folder structure looks like following:

```
    Economic Data
                |_ data
                       |_ AUD
                       |_ CAD
                       |_ ...
```

# Usage

Use as any indicator for MetaTrader. Drop ex4 file to your indicators directory and refresh indicator list in the terminal

# Indicator Buffers

There're 3 buffers defined within the indicator:

- `0` Minutes left until closest event with Low volatility expected
- `1` Minutes left until closest event with Moderate volatility expected
- `2` Minutes left until closest event with High volatility expected

# Indicator Parameters

- `Include Low Volatility Events` - should the events with `Low` expected volatility appear in the buffer `0`
- `Include Moderate Volatility Events` - should the events with `Moderate` expected volatility appear in the buffer `1`
- `Include High Volatility Events` - should the events with `High` expected volatility appear in the buffer `2`
- `Path to the location of events data` - folder with events data see [Pre-requisites to use](#pre-requisites-to-use)
- `Broker Time Zone` - broker time zone or time zone of the data used for back testing. Needed to be set correctly in order to calculate events time. Source data contains the events in the GMT time zone and therefore this parameter is important.
- `Visualize Events` - weather or not put vertical lines in case of event to the main chart
- `Log level output` - the level of the logger output (`TRACE(0)`, `DEBUG(1)`, `INFO(2)`, `WARN(3)`, `ERROR(4)`, `CRITICAL(5)`), where `TRACE` is the most detailed log an `CRITICAL` is least detailed.
- `Log spool to journal` - should the logger write to journal or a separate log file (`true` - journal, `falase` - separate file, in the latter case log file is available in `Files` direcotry of a tester or termina data folder respectively)

# Example Usage

```cpp
#define NEWS_LOW_VOLATILITY_BUFFER_ID 0 // Buffer for LOW volatility news
#define NEWS_MODERATE_VOLATILITY_BUFFER_ID 1 // Buffer for MODERATE volatility news
#define NEWS_HIGH_VOLATILITY_BUFFER_ID 2 // Buffer for HIGH volatility news
#define ECONOMIC_DATA_FOLDER "Economic Data\\data\\" // Folder that contains data
#define NEWS_INCLUDE_LOW_VOLATILITY false; // Include LOW volatility news
#define NEWS_INCLUDE_MODERATE_VOLATILITY true; // Include MODERATE volatility news
#define NEWS_INCLUDE_HIGH_VOLATILITY false; // Include HIGH volatility news
#define BROKER_TIME_ZONE 2 //GTM+2, for GMT-3 use -3
#define VISUALIZE_EVENTS true // Visualize events on the cart
#define LOG_LEVEL 1 // Log level
#define SPOOL_TO_CONSOLE false // Redirect log output to console

...

double minutesToLowVolatilityEvent=iCustom(_Symbol, _Period, newsIndicatorLocation, NEWS_INCLUDE_LOW_VOLATILITY,NEWS_INCLUDE_MODERATE_VOLATILITY,NEWS_INCLUDE_HIGH_VOLATILITY,ECONOMIC_DATA_FOLDER, BROKER_TIME_ZONE, VISUALIZE_EVENTS, LOG_LEVEL, SPOOL_TO_CONSOLE,  NEWS_LOW_VOLATILITY_BUFFER_ID,0);
...
double minutesToModerateVolatilityEvent=iCustom(_Symbol, _Period, newsIndicatorLocation, NEWS_INCLUDE_LOW_VOLATILITY,NEWS_INCLUDE_MODERATE_VOLATILITY,NEWS_INCLUDE_HIGH_VOLATILITY,ECONOMIC_DATA_FOLDER, BROKER_TIME_ZONE, VISUALIZE_EVENTS, LOG_LEVEL, SPOOL_TO_CONSOLE,  NEWS_MODERATE_VOLATILITY_BUFFER_ID,0);
...
double minutesToHighVolatilityEvent=iCustom(_Symbol, _Period, newsIndicatorLocation, NEWS_INCLUDE_LOW_VOLATILITY,NEWS_INCLUDE_MODERATE_VOLATILITY,NEWS_INCLUDE_HIGH_VOLATILITY,ECONOMIC_DATA_FOLDER, BROKER_TIME_ZONE, VISUALIZE_EVENTS, LOG_LEVEL, SPOOL_TO_CONSOLE, NEWS_HIGH_VOLATILITY_BUFFER_ID,0);
```

# Credits

[MQL4 Lib](https://github.com/dingmaotu/mql4-lib) it always makes my life so much easier
[fxsabber](https://www.mql5.com/en/forum/312388#comment_11532277) brilliant idea for sorting arrays of structures

# License

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>