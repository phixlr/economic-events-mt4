#property strict
#include <Files/File.mqh>

//+------------------------------------------------------------------+
//| Log Enumeration Level                                            |
//+------------------------------------------------------------------+
enum ENUM_LOG_LEVEL
{
  TRACE = 0,
  DEBUG = 1,
  INFO = 2,
  WARN = 3,
  ERROR = 4,
  CRITICAL = 5
};

//+------------------------------------------------------------------+
//| Class Logger                                                   |
//| Purpose: Class of operations with log files.                     |
//|          Derives from class CFile.                               |
//+------------------------------------------------------------------+

class Logger : public CFile
{
private:
  ENUM_LOG_LEVEL m_level;
  bool m_print;

public:
  Logger();
  Logger(string filename = "Logger.log", ENUM_LOG_LEVEL level = 3, bool print = false);
  ~Logger();
  //--- methods for working with files
  void SetLevel(ENUM_LOG_LEVEL value) { m_level = value; }
  void SetPrint(bool value) { m_print = value; }
  //--- methods for working with files
  int Open(const string file_name, const int open_flags);
  //--- methods to access data
  uint Write(const string value);
  uint Log(const ENUM_LOG_LEVEL level, const string value);
  void TRACE(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "");
  void DEBUG(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "");
  void INFO(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "");
  void WARN(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "");
  void ERROR(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "");
  void CRITICAL(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "");
};

//+------------------------------------------------------------------+
//| Standard Constructor                                             |
//+------------------------------------------------------------------+
Logger::Logger() : m_level(WARN),
                   m_print(false)
{
}

//+------------------------------------------------------------------+
//| Constructor with options                                         |
//| @var string file_name                                            |
//|             Filename of LogFile with extension                   |
//| @var ENUM_LOG_LEVEL level                                        |
//|             Level of Log message                                 |
//| @var bool print                                                  |
//|             print to console                                     |
//+------------------------------------------------------------------+
Logger::Logger(string file_name, ENUM_LOG_LEVEL level = 3, bool print = false)
{
  Open(file_name);
  SetLevel(level);
  SetPrint(print);
}

//+------------------------------------------------------------------+
//|  Deconstructor                                                   |
//+------------------------------------------------------------------+
Logger::~Logger()
{
}

//+------------------------------------------------------------------+
//| Open the log file                                                |
//| @var string file_name                                            |
//|             Filename of LogFile with extension                   |
//| @var int open_flags                                              |
//|             Flags for Opening LogFile                            |
//| @return int                                                      |
//+------------------------------------------------------------------+
int Logger::Open(const string file_name, const int open_flags = FILE_WRITE)
{
  return (CFile::Open(file_name, open_flags | FILE_CSV));
}

//+------------------------------------------------------------------+
//| Writing string to file                                           |
//| @var string value                                                |
//|             Message to Write                                     |
//+------------------------------------------------------------------+
uint Logger::Write(const string value)
{
  if (m_print)
  {
    Print(value);
  }
  //--- check handle
  if (m_handle != INVALID_HANDLE)
    return (FileWrite(m_handle, TimeToStr(TimeCurrent(), TIME_DATE | TIME_MINUTES | TIME_SECONDS), "", value));
  //--- failure
  return (0);
}

//+------------------------------------------------------------------+
//| Log string to file                                               |
//| @var ENUM_LOG_LEVEL level                                        |
//|             Level of Log message                                 |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return uint                                                     |
//|             0 returned on failure                                |
//+------------------------------------------------------------------+
uint Logger::Log(const ENUM_LOG_LEVEL level, const string value)
{
  string levelString = StringFormat("[%s]", EnumToString(level));
  if (m_print && m_level <= level)
  {
    PrintFormat("%s %s", levelString, value);
  }
  //--- check handle
  if (m_handle != INVALID_HANDLE && m_level <= level)
    return (FileWrite(m_handle, TimeToStr(TimeCurrent(), TIME_DATE | TIME_MINUTES | TIME_SECONDS), levelString, value));
  //--- failure
  return (0);
}

//+------------------------------------------------------------------+
//| Writing Trace string to file                                     |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return void                                                     |
//+------------------------------------------------------------------+
void Logger::TRACE(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "")
{
  Log(TRACE, StringConcatenate(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10,value11, value12, value13, value14, value15));
}
//+------------------------------------------------------------------+
//| Writing Debug string to file                                     |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return void                                                     |
//+------------------------------------------------------------------+
void Logger::DEBUG(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "")
{
  Log(DEBUG, StringConcatenate(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10,value11, value12, value13, value14, value15));
}
//+------------------------------------------------------------------+
//| Writing Info string to file                                      |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return void                                                     |
//+------------------------------------------------------------------+
void Logger::INFO(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "")
{
  Log(INFO, StringConcatenate(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10,value11, value12, value13, value14, value15));
}
//+------------------------------------------------------------------+
//| Writing Warning string to file                                   |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return void                                                     |
//+------------------------------------------------------------------+
void Logger::WARN(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "")
{
  Log(WARN, StringConcatenate(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10,value11, value12, value13, value14, value15));
}
//+------------------------------------------------------------------+
//| Writing Error string to file                                     |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return void                                                     |
//+------------------------------------------------------------------+
void Logger::ERROR(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "")
{
  Log(ERROR, StringConcatenate(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10,value11, value12, value13, value14, value15));
}
//+------------------------------------------------------------------+
//| Writing Critical string to file                                  |
//| @var string value                                                |
//|             Message to Write                                     |
//| @return void                                                     |
//+------------------------------------------------------------------+
void Logger::CRITICAL(const string value1 = "", const string value2 = "", const string value3 = "", const string value4 = "", const string value5 = "", const string value6 = "", const string value7 = "", const string value8 = "", const string value9 = "", const string value10 = "", const string value11 = "", const string value12 = "", const string value13 = "", const string value14 = "", const string value15 = "")
{
  Log(CRITICAL, StringConcatenate(value1, value2, value3, value4, value5, value6, value7, value8, value9, value10,value11, value12, value13, value14, value15));
}

//+------------------------------------------------------------------+