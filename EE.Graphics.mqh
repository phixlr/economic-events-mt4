#define VISUAL_PREFIX "economic-event-"
#define COLOR_EVENT_LOW clrGreen
#define COLOR_EVENT_MODERATE clrYellow
#define COLOR_EVENT_HIGH clrRed

void VisualizeEvents(EconomicEvent &economicEvents[])
{
    for (int i = 0; i < ArraySize(economicEvents); i++)
    {
        if (economicEvents[i].expectedVolatility == LOW)
        {
            log.DEBUG(__FUNCTION__, " No objects found at time: ", TimeToString(economicEvents[i].eventDateTime));
            DrawVerticalLine(StringConcatenate(VISUAL_PREFIX, economicEvents[i].eventName),
                             economicEvents[i].eventDateTime,
                             COLOR_EVENT_LOW,
                             1,
                             StringConcatenate("(*) ", economicEvents[i].eventName));
        }
        else if (economicEvents[i].expectedVolatility == MODERATE)
        {
            log.DEBUG(__FUNCTION__, " No objects found at time: ", TimeToString(economicEvents[i].eventDateTime));
            DrawVerticalLine(StringConcatenate(VISUAL_PREFIX, economicEvents[i].eventName),
                             economicEvents[i].eventDateTime,
                             COLOR_EVENT_MODERATE,
                             2,
                             StringConcatenate("(**) ", economicEvents[i].eventName));
        }
        else if (economicEvents[i].expectedVolatility == HIGH)
        {
            log.DEBUG(__FUNCTION__, " No objects found at time: ", TimeToString(economicEvents[i].eventDateTime));
            DrawVerticalLine(StringConcatenate(VISUAL_PREFIX, economicEvents[i].eventName),
                             economicEvents[i].eventDateTime,
                             COLOR_EVENT_HIGH,
                             3,
                             StringConcatenate("(***) ", economicEvents[i].eventName));
        }
    }
}

void ClearEventsVizualizations()
{
    ObjectsDeleteAll(ChartID(), VISUAL_PREFIX);
}

//+------------------------------------------------------------------+
//| Create the vertical line                                         |
//+------------------------------------------------------------------+
void DrawVerticalLine(const string name = "economic-event-vertical-line", // line name
                      datetime time = 0,                                  // line time
                      const color clr = clrRed,                           // line color
                      const int width = 1,                                // line width
                      const string descr = ""                            // line desctiption
                      )

{
    const long chart_ID = 0;                   // chart's ID
    const int sub_window = 0;                  // subwindow index
    const ENUM_LINE_STYLE style = STYLE_SOLID; // line style
    const bool back = false;                   // in the background
    const bool selection = false;              // highlight to move
    const bool selectable = true;              // Is selectable
    const bool hidden = false;                 // hidden in the object list
    const long z_order = 0;                    // priority for mouse click

    if (ObjectCreate(chart_ID, name, OBJ_VLINE, sub_window, time, 0))
    {
        //--- set line color
        ObjectSetInteger(chart_ID, name, OBJPROP_COLOR, clr);
        //--- set line display style
        ObjectSetInteger(chart_ID, name, OBJPROP_STYLE, style);
        //--- set line width
        ObjectSetInteger(chart_ID, name, OBJPROP_WIDTH, width);
        //--- display in the foreground (false) or background (true)
        ObjectSetInteger(chart_ID, name, OBJPROP_BACK, back);
        //--- enable (true) or disable (false) the mode of moving the line by mouse
        //--- when creating a graphical object using ObjectCreate function, the object cannot be
        //--- highlighted and moved by default. Inside this method, selection parameter
        //--- is true by default making it possible to highlight and move the object
        ObjectSetInteger(chart_ID, name, OBJPROP_SELECTABLE, selectable);
        ObjectSetInteger(chart_ID, name, OBJPROP_SELECTED, selection);
        //--- hide (true) or display (false) graphical object name in the object list
        ObjectSetInteger(chart_ID, name, OBJPROP_HIDDEN, hidden);
        //--- set the priority for receiving the event of a mouse click in the chart
        ObjectSetInteger(chart_ID, name, OBJPROP_ZORDER, z_order);
        //ObjectSetString(chart_ID,name,OBJPROP_TEXT,descr);
        ObjectSetString(chart_ID, name, OBJPROP_TOOLTIP, descr);
        ObjectSetString(chart_ID, name, OBJPROP_TEXT, descr);
        //--- successful execution
    }
    else
    {
        log.ERROR("Error: can't vertical line with name: ", name, " at time: ", TimeToString(time), " error code: ", IntegerToString(GetLastError()));
        return;
    }
}