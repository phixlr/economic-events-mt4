enum ExpectedVolatility
{
  UNKNOWN=0,
  LOW=1,
  MODERATE=2,
  HIGH=3
};

struct EconomicEvent
{
  string eventName;
  string eventCountry;
  string eventCurrency;
  datetime eventDateTime;
  string eventTimeZone;
  string eventPrevious;
  string eventForecast;
  string eventActual;
  ExpectedVolatility expectedVolatility;
};

string EconomicEventToString(EconomicEvent &event)
{
  return StringConcatenate("Event name: ", event.eventName,
                           " Event Currency: ", event.eventCurrency,
                           " Event Date and Time: ", TimeToString(event.eventDateTime),
                           " Event time zone: ", event.eventTimeZone,
                           " Event previous value: ", event.eventPrevious,
                           " Event Forecast value: ", event.eventForecast,
                           " Event actual: ", event.eventActual,
                           " Expected volatility: ", EnumToString(event.expectedVolatility));
}

ExpectedVolatility ExpectedVolatilityFromString(string expectedVolatilityString)
{
  log.DEBUG(__FUNCTION__, " Parameter: ", expectedVolatilityString);

  if (StringCompare("Low", expectedVolatilityString, false) == 0)
  {
    log.DEBUG(__FUNCTION__, " Low volatility detected.");
    return LOW;
  }
  else if (StringCompare("Moderate", expectedVolatilityString, false) == 0)
  {
    log.DEBUG(__FUNCTION__, " Moderate volatility detected.");
    return MODERATE;
  }
  else if (StringCompare("High", expectedVolatilityString, false) == 0)
  {
    log.DEBUG(__FUNCTION__, " High volatility detected.");
    return HIGH;
  }
  log.DEBUG(__FUNCTION__, " Volatility can not be determined.");
  return UNKNOWN;
}

enum TimeZone
{
  GMT = 0,            // GMT
  GMT_PLUS_1 = 1,     //GMT+1
  GMT_PLUS_2 = 2,     //GMT+2
  GMT_PLUS_3 = 3,     //GMT+3
  GMT_PLUS_4 = 4,     //GMT+4
  GMT_PLUS_5 = 5,     //GMT+5
  GMT_PLUS_6 = 6,     //GMT+6
  GMT_PLUS_7 = 7,     //GMT+7
  GMT_PLUS_8 = 8,     //GMT+8
  GMT_PLUS_9 = 9,     //GMT+9
  GMT_PLUS_10 = 10,   //GMT+10
  GMT_PLUS_11 = 11,   //GMT+11
  GMT_PLUS_12 = 12,   //GMT+12
  GMT_MINUS_1 = -1,   //GMT-1
  GMT_MINUS_2 = -2,   //GMT-2
  GMT_MINUS_3 = -3,   //GMT-3
  GMT_MINUS_4 = -4,   //GMT-4
  GMT_MINUS_5 = -5,   //GMT-5
  GMT_MINUS_6 = -6,   //GMT-6
  GMT_MINUS_7 = -7,   //GMT-7
  GMT_MINUS_8 = -8,   //GMT-8
  GMT_MINUS_9 = -9,   //GMT-9
  GMT_MINUS_10 = -10, //GMT-10
  GMT_MINUS_11 = -11, //GMT-11
  GMT_MINUS_12 = -12  //GMT-12
};